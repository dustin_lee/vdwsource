import math as ma
import numpy as np
from numpy.linalg import norm

import gpaw
from gpaw.utilities.ps2ae import PS2AE

from units import *


#  Get the electrostatic potential.
material_name = "wse2"
material, calc = gpaw.restart(f"materials/data/{material_name}.gpw")
transformer = PS2AE(calc)
potential = transformer.get_electrostatic_potential(ae=True) #In eV.

# Compute the electric field.
# First, some variables that are used are computed.
basis_change_to_cartesian = np.transpose(np.array([material.cell[0],
                                                   material.cell[1],
                                                   material.cell[2]]))
basis_change_from_cartesian = np.linalg.inv(basis_change_to_cartesian)
step_sizes = [norm(material.cell[i])/(potential.shape[i]-1) * angstrom
              for i in range(3)]
# The electric field is the gradient of the potential.
field = np.zeros(potential.shape + (3,))
for idx in np.ndindex(potential.shape):
    grad = np.zeros(3)
    # Compute the gradient in the given basis.
    for i in range(3):
        prev_idx = list(idx)
        next_idx = list(idx)
        if idx[i] == 0:
            prev_idx[i] = -1
            next_idx[i] += 1
        elif idx[i]+1 >= potential.shape[i]:
            prev_idx[i] -= 1
            next_idx[i] = 0
        else: #This is the most expected case.
            prev_idx[i] -= 1
            next_idx[i] += 1
        prev_idx = tuple(prev_idx)
        next_idx = tuple(next_idx)
        grad[i] = (potential[next_idx]-potential[prev_idx])/step_sizes[i]
    # Change the basis.
    grad = basis_change_to_cartesian @ grad
    # Add the field slice.
    field[idx] = -grad

# Save the electric field.
np.save(f"materials/data/field_{material_name}.npy", field)
