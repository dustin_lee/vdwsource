import copy
import math as ma
import numpy as np
import os

import ase
import ase.io as io
import ase.visualize as vis
import gpaw

import warnings
warnings.filterwarnings("ignore")


material_name = "wse2"

# Paramters.
num_layers = 5
vert_padding = 12.0
layer_spacing = 3.0 #Is this a guess?
# Positions vectors.
vec_len = 3.297
vec1 = np.array([vec_len, 0., 0.])
angle = ma.radians(60.)
vec2 = vec_len*np.array([ma.cos(angle), ma.sin(angle), 0.])
hyp = (vec_len/2.)/ma.sin(ma.radians(60.))
side_len_half = hyp*ma.sin(ma.radians(30.))
sum_norm = np.linalg.norm(vec1+vec2)
unit_sum = (vec1 + vec2)/sum_norm
side_len = sum_norm/3.
position1 = side_len*unit_sum
position2 = 2.*side_len*unit_sum
# Bonds.
bond_len = 2.526
bond_angle = ma.acos(side_len/bond_len)
height = bond_len*ma.sin(bond_angle)

# Place the atoms.
se_positions = []
w_positions = []
cur_height = 0.
for i in range(num_layers):
    for j in range(3):
        if i%2 == 0 and j%2 == 0:
            position = position1.copy()
        elif i%2 == 0 and j%2 != 0:
            position = position2.copy()
        elif i%2 != 0 and j%2 == 0:
            position = position2.copy()
        else:
            position = position1.copy()
        position[2] = cur_height
        if j%2 == 0:
            se_positions.append(position)
        else:
            w_positions.append(position)
        if j != 2:
            cur_height += height
    cur_height += layer_spacing
positions = se_positions + w_positions
num_se_atoms = len(se_positions)
num_w_atoms = len(w_positions)
cell_height = num_layers*(2*height + layer_spacing) + 2.*vert_padding
cell = ase.cell.Cell([vec1, vec2, [0.0, 0.0, cell_height]])

# Make the material.
material = ase.Atoms(f"Se{num_se_atoms}W{num_w_atoms}",
                     positions=positions,
                     cell=cell,
                     pbc=True)
material.center()

# View the material.
vis.view(material, repeat=(6, 6, 1))

# Run the calculator.
material.calc = gpaw.GPAW(mode=gpaw.PW(300))
potential = material.get_potential_energy()

# Save the data.
if not os.path.isdir("materials/data"):
    os.mkdir("materials/data")
io.write("materials/data/wse2.xyz", material)
material.calc.write("materials/data/wse2.gpw", mode="all")
