import math as ma
import numpy as np
import os
import scipy as sp

from units import *
from constants import *
from parameters import *
import trajectory as traj
import fields


# Get the trajectory.
electron_locations = traj.comp_traj()

# Intialize the data.
times = np.zeros((num_divs, num_divs, samples_per_pt))
electric_field = np.zeros((num_divs, num_divs, samples_per_pt, 3))
magnetic_field = np.zeros((num_divs, num_divs, samples_per_pt, 3))
amplitudes = np.zeros((num_divs, num_divs, samples_per_pt))
energies = np.zeros((num_divs, num_divs, samples_per_pt))

# Compute the data.
num_divs_computed = 0
for i, j in np.ndindex((num_divs, num_divs)):
    x = observation_locations[i, j, 0]
    y = observation_locations[i, j, 1]
    observation_location = detector_location + np.array([x, y, 0.])
    field_info = fields.comp_fields(electron_locations, observation_location)
    times[i, j, :] = field_info[0]
    electric_field[i, j, :, :] = field_info[1]
    magnetic_field[i, j, :, :] = field_info[2]
    # Convert the electric field to the frequency domain.
    x_comps = electric_field[i, j, :, 0]
    amplitudes[i, j, :] = sp.fft.fft(x_comps)
    d = times[i, j, 1] - times[i, j, 0]
    freqs = sp.fft.fftfreq(amplitudes.shape[-1], d=d)
    energies[i, j, :] = planck_const*freqs #Rescale.
    # Print progress.
    num_divs_computed += 1
    print("Fields have been computed for "
          f"{num_divs_computed}/{num_divs**2} divisions.")

# Save the data.
if not os.path.isdir("results/data"):
    os.mkdir("results/data")
np.save("results/data/times.npy", times)
np.save("results/data/electricfield.npy", electric_field)
np.save("results/data/magneticfield.npy", magnetic_field)
np.save("results/data/amplitudes.npy", amplitudes)
np.save("results/data/energies.npy", energies)
