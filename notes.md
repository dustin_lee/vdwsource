# Notes for the Simulation of Free Electrons in van-der-Waals Materials

## To-Do List


**Today**
* Make sure that the result programs are correct.
  - Intensity.
  - Spectrum.
  - Brilliance.
* Add everything to the git.


* It might be pointless to save xyz files.
* Are the calculations for the Lorentz factor and whatnot correct?
* The test to plot potential.
  * Remove ```plotdensity.py```.
* Make the process ready for multiple materials.
* Put in the part about materials in ```readme.md```.
  - Put in the process.
* Deal with the potential stuff.
  - Does it make sense for potential to be in electron volts? Do they just mean volts?
* Correctly deal with the Fourier transform.
  - I guess just do it for each component?
* The test to plot sample spectra.
* Can I add the other tests and have them be reasonable?
  - Plot the trajectory with ```plottrajectory.py```.
  - Do I need to deal with the electron spacing?
  - Should I plot sample restrictions?

* Play with the opening angle.
  - Currently, I just have a place holder value.
* Set it up to deal with different electron energies.
* Set it up to deal with multiple electron start locations.
* Simulate a steady state.


**Later**
* Am I sure that there are two layers of selenium between the layers of tungsten?
* What is a reasonable electron energy and a reasonable current?
* Deal with the relaxation.
  - Try starting the layers closer together.
    * How do I vary the cell size?
  - Check that symmetry isn't a problem.
* Make a GIF for electron density?


**More**
* Add in parametric xrays.


## General

* The electrons in the beam might be spaced far enough that a steady state doesn't really need to be simulated.


## To Understand about ASE and GPAW

* Understand relaxation:
  - The positions are only changing vertically. Thus, I think that the cell is not able to change sizes.
* Understand the parallel stuff.
  - For parallel computation, run "mpiexec -np 6 python aseguide.py".
* Figure out how to use the built-in database.
