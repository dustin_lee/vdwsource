import math as ma
import numpy as np
import scipy as sp
from scipy.interpolate import interp1d

from units import *
from constants import *
from parameters import *
import trajectory as traj


def comp_fields_for_one_electron(electron_locations, observation_location):
    # This computes the fields using the Heaviside-Feynman Formulas.

    # Get the data to compute the electric field.
    num_samples = len(electron_locations)
    displacements = observation_location - electron_locations
    distances = (displacements**2.).sum(axis=-1)**.5
    directions = observation_location-electron_locations
    directions /= distances[:, np.newaxis]

    # Compute the electric field.
    electric_field = np.zeros((num_samples-2, 3))
    # The first term.
    terms1 = directions[1:-1]/(distances[1:-1, np.newaxis]**2.)
    # The second term.
    terms2 = distances[1:-1]/speed_of_light
    ratios = directions/(distances[:, np.newaxis]**2.)
    terms2 = terms2[:, np.newaxis] * (ratios[2:] - ratios[:-2])/(2.*time_step)
    # The third term.
    terms3 = np.ones((num_samples-2, 3))/(speed_of_light**2.)
    terms3 *= directions[2:] - 2.*directions[1:-1] + directions[:-2]
    terms3 /= time_step**2.
    # Combine the terms.
    electric_field = electron_charge/(4.*ma.pi*eps0)*(terms1 + terms2 + terms3)

    # Get the magnetic field.
    magnetic_field = -np.cross(directions[1:-1], electric_field)/speed_of_light

    # Get the observations times.
    emit_times = np.linspace(0., time_step*(num_samples-1), num_samples)
    observation_times = emit_times + distances/speed_of_light
    observation_times = observation_times[1:-1] #To make its length the same
    observation_times -= observation_times[0]         #as the length of fields.

    # Interpolate the fields.
    E_interp = interp1d(observation_times.copy(), electric_field, axis=0,
                        assume_sorted=True)
    B_interp = interp1d(observation_times.copy(), magnetic_field, axis=0,
                        assume_sorted=True)
    observation_times = np.linspace(observation_times[0],
                                    observation_times[-1],
                                    samples_per_pt)
    electric_field = E_interp(observation_times)
    magnetic_field = B_interp(observation_times)

    return observation_times, electric_field, magnetic_field


def comp_fields(electron_locations, observation_location):
    # Later this will adjust for interference.

    field_info = comp_fields_for_one_electron(electron_locations,
                                              observation_location)
    observation_times, electric_field, magnetic_field = field_info

    # Fill this in.

    return observation_times, electric_field, magnetic_field


def comp_intensity(E, B, times):
        # Integrate to get the total energy.
        intensity = (eps0*E*E/2. + mu0*B*B/2.)
        if E.shape == (num_divs, num_divs, samples_per_pt, 3):
            intensity = intensity.sum(axis=-1).sum(axis=-1)
        elif E.shape == (num_divs, num_divs, samples_per_pt):
            intensity = intensity.sum(axis=-1) #It is just the x-component.
        intensity *= ((times[:, :, 1]-times[:, :, 0])*speed_of_light) * sample_area

        # Scale by current.
        time_widths = times[:, :, -1] - times[:, :, 0]
        intensity /= time_widths

        return intensity


def get_restricted_fields(energies, amplitudes, lower_bound, upper_bound):
    energies = energies.copy()
    amplitudes = amplitudes.copy()

    # Filter for these energy bounds.
    indices = np.argwhere(((energies>upper_bound) | (energies<lower_bound))
                          &((energies<-upper_bound) | (energies>-lower_bound)))
    amplitudes[indices[:, 0], indices[:, 1], indices[:, 2]] = 0.

    # Take the inverse Fourier transform to get the electric field.
    electric_field = sp.fft.ifft(amplitudes)
    # Get the magnetic field.
    magnetic_field = electric_field/speed_of_light

    return electric_field, magnetic_field


def comp_num_photons(lower_bound, upper_bound,
                     times, energies, amplitudes):
    both_fields = get_restricted_fields(energies, amplitudes,
                                        lower_bound, upper_bound)
    electric_field, magnetic_field = both_fields
    intensity = comp_intensity(electric_field, magnetic_field, times)
    total_energy = intensity.sum()
    mean_energy = (lower_bound+upper_bound)/2.
    num_photons = total_energy/mean_energy
    return float(num_photons)
