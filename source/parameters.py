import math as ma
import numpy as np

import ase
import ase.io as io

from units import *
from constants import *


# Electron parameters.
dist_to_detector = 30. * meter
electron_kinetic_energy = 10.*keV
electron_rest_energy = electron_mass*speed_of_light**2.
start_energy = electron_kinetic_energy + electron_rest_energy
current = 1. * milliamp
start_speed = (speed_of_light
               *(1. - (electron_mass*speed_of_light**2./start_energy)**2.)**.5)
lorentz_factor = 1./ma.sqrt(1. - (start_speed/speed_of_light)**2)

# Material parameters.
material_name = "wse2"
material = io.read(f"materials/data/{material_name}.xyz")
field_in_material = np.load(f"materials/data/field_{material_name}.npy")
cell_thickness = material.cell[2][-1] * angstrom
#Change this: Use the material to get the angle.
angle = ma.radians(60.)
projx = ma.cos(angle)
projy = ma.sin(angle)

# Dependent parameters.
opening_angle = .5*mrad #This is just a filler for now.
central_cone_radius = dist_to_detector * ma.tan(opening_angle/2.)
detector_radius = 3. * central_cone_radius
central_cone_area = ((2.*central_cone_radius))**2.
detector_radius = 5. * cm
detector_location = np.array([0., 0., dist_to_detector + cell_thickness/2.])

# Parameters for the numerical methods.
# Parameters for computing the trajectory.
num_samples = 2000
time_step = (cell_thickness/speed_of_light)/num_samples
# Sampling at the detector.
samples_per_pt = 8192
num_divs = 51 #Number of divisions per side.
sample_area = (2.*detector_radius/num_divs)**2.
assert num_divs%2 == 1
x_observation_coords = np.linspace(-detector_radius, detector_radius, num_divs)
y_observation_coords = np.linspace(-detector_radius, detector_radius, num_divs)
observation_locations = np.zeros((num_divs, num_divs, 2))
for i, x in enumerate(x_observation_coords):
    for j, y in enumerate(y_observation_coords):
        observation_locations[i, j, :] = np.array([x, y])
distances_from_center = (observation_locations**2.).sum(-1)**.5

# Print some parameters.
print("The kinetic energy of the electron "
      + f"is {electron_kinetic_energy/keV :.5} keV.")
print("The thickness of the cell of the material "
      f"is {cell_thickness/angstrom :.3} Å.")
