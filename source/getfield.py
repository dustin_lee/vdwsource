import math as ma
import numpy as np
from numpy.linalg import norm
from scipy.interpolate import RegularGridInterpolator

import ase
import ase.io as io

from units import *
from constants import *
from parameters import *


# These variables are global to avoid calculating each time that
# the function get_electric_field is called.
# First, the change of basis matrices are computed.
basis_change_to_cartesian = np.transpose(np.array([material.cell[0]*angstrom,
                                                   material.cell[1]*angstrom,
                                                   material.cell[2]*angstrom]))
basis_change_from_cartesian = np.linalg.inv(basis_change_to_cartesian)
# Now, the interpolator is made.
locations = np.zeros(field_in_material.shape)
X = (np.linspace(0., norm(material.cell[0]), field_in_material.shape[0])
     * angstrom)
Y = (np.linspace(0., norm(material.cell[1]), field_in_material.shape[1])
     * angstrom)
Z = (np.linspace(0., norm(material.cell[2]), field_in_material.shape[2])
     * angstrom)
get_electric_field_in_cell = RegularGridInterpolator((X, Y, Z),
                                                     field_in_material)


def put_in_primitive_cell(location):
    location = basis_change_from_cartesian @ location
    location[0] %= 1.
    location[1] %= 1.
    if location[0] == 1.:
        location[0] = 0.
    if location[1] == 1.:
        location[1] = 0.
    location = basis_change_to_cartesian @ location
    return location


def get_field_in_material(location):
    location = put_in_primitive_cell(location)
    electric_field = get_electric_field_in_cell(location)
    electric_field = electric_field.squeeze()
    return electric_field
