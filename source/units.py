# Fundamental units.
meter = 1.
kg = 1.
sec = 1.
coulomb = 1.
rad = 1.

# Derived units.
# New units.
joule = 1.
volt = 1.
amp = 1.
# Prefixed units.
angstrom = 1e-10 * meter
mm = 1e-3 * meter
cm = 1e-2 * meter
nm = 1e-9 * meter
femtosec = 1e-15 * sec
eV = 1.602176634e-19 * joule
keV = 1e3 * eV
mV = 1e-6 * volt
mrad = 1e-3 * rad
milliamp = 1e-3 * amp
