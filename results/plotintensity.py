import matplotlib
import matplotlib.pyplot as plt
import numpy as np

from units import *
from constants import *
from parameters import *
import fields


# Get the data.
times = np.load("results/data/times.npy")
electric_field = np.load("results/data/magneticfield.npy")
magnetic_field = np.load("results/data/magneticfield.npy")

# Plot the intensities.
intensities = fields.comp_intensity(electric_field, magnetic_field, times)
# Make a heat map of the intensity.
X, Y = np.meshgrid(x_observation_coords, y_observation_coords)
fig, ax = plt.subplots()
color_bar = ax.pcolormesh(X/mm, Y/mm, intensities/keV,
                          cmap="hot", shading="gouraud")
fig.colorbar(color_bar, ax=ax,
             label="intensity (keV/mm^2)")
# Display the plot.
plt.xlabel("horizontal displacement (mm)")
plt.ylabel("vertical displacement (mm)")
plt.show()
