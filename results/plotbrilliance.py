import matplotlib.pyplot as plt
import numpy as np

from units import *
from constants import *
from parameters import *
import fields


# Get the data
times = np.load("results/data/times.npy")
energies = np.load("results/data/energies.npy")
amplitudes = np.load("results/data/amplitudes.npy")

# Compute the simulated brilliances at various energies.
central_energies = np.linspace(1.*keV, energies.max(), 300)
brilliances = np.zeros(central_energies.size)
for i, energy in enumerate(central_energies):
    width = .001 * energy
    brilliance = fields.comp_num_photons(energy-width/2., energy+width/2.,
                                         times, energies, amplitudes)
    brilliance /= (opening_angle/mrad)**2.
    brilliance /= central_cone_area/mm**2.
    brilliances[i] = brilliance
# Plot the brilliances.
plt.plot(central_energies/keV, brilliances)
plt.xlabel("energy (keV)")
plt.ylabel("brilliance ((count/s)/(mm^2⋅mrad^2))")
plt.xlim(1.)
plt.show()
