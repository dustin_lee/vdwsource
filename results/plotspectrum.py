import matplotlib.pyplot as plt
import numpy as np

from units import *
from constants import *
from parameters import *
import fields


# Get the data.
times = np.load("results/data/times.npy")
energies = np.load("results/data/energies.npy")
amplitudes = np.load("results/data/amplitudes.npy")

# Compute the spectrum.
domain = np.linspace(1.*keV, energies.max(), 300)
bin_len = domain[1] - domain[0]
image = []
for energy in domain:
    restricted_fields = fields.get_restricted_fields(energies, amplitudes,
                                                     energy-bin_len/2.,
                                                     energy+bin_len/2.)
    electric_field, magnetic_field = restricted_fields
    intensity = fields.comp_intensity(electric_field, magnetic_field, times)
    total_energy = intensity.sum()
    deposited_energy_density = total_energy/bin_len
    image.append(deposited_energy_density)

# Plot the spectrum.
plt.plot(domain/keV, image)
plt.xlabel("photon energy (keV)")
plt.ylabel("deposited-energy density per second (1/s)")
plt.xlim(0.)
plt.ylim(0.)
plt.show()
