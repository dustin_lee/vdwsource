import numpy as np
import math as ma

import ase
import ase.io

import matplotlib.pyplot as plt

import dims


def make_grid(shape, vec1, vec2):
    shape = shape[:2]

    norm = np.linalg.norm(vec1)
    angle = ma.radians(60.)
    projx = ma.cos(angle)
    projy = ma.sin(angle)

    x = []
    y = []
    for comp1 in np.linspace(0., norm, shape[0]):
        for comp2 in np.linspace(0., norm, shape[1]):
            x.append(comp1 + projx*comp2)
            y.append(projy*comp2)

    X = np.array(x).reshape(shape)
    Y = np.array(y).reshape(shape)

    return X, Y


material = ase.io.read("data/wse2.xyz")
density = np.load("data/density_wse2.npy")

heights = [atom.position[2] for atom in material]
zdim = material.cell[2][2]
step_dist = zdim/density.shape[2]
bottom_height = min(heights)
top_height = max(heights)
start_idx = int(bottom_height/step_dist) - 5
end_idx = int(top_height/step_dist) + 5

X, Y = make_grid(density.shape, dims.vec1, dims.vec2)

fig = plt.figure()
fig.suptitle("Electron Density in WSe2")

jump = 5
for i in range(start_idx, start_idx + 9*jump, jump): #end_idx):
    density_slice = density[:,:,i]

    idx = (i - start_idx)//jump + 1
    ax = fig.add_subplot(3, 3, idx, projection='3d')
    ax.plot_surface(X, Y, density_slice)
    # ax.set_zlim(0., 200.)
    height = i*step_dist - bottom_height
    height = round(height, 2)
    ax.set_title(f"height {height} Å")
    if idx > 6:
        ax.set_xlabel("x (Å)")
        ax.set_ylabel("y (Å)")
    if idx%3 == 0:
        ax.set_zlabel("electron density (1/Å^3)")

plt.show()
