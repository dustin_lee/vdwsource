import math as ma
import numpy as np
from numpy.linalg import norm

import ase
import ase.io as io

import matplotlib.pyplot as plt


material_name = "wse2"
material = io.read(f"data/{material_name}.xyz")
potential = np.load(f"data/potential_{material_name}.npy")
field = np.load(f"data/field_{material_name}wse2.npy")

vertical_index = 120

# Make the domain.
angle = ma.radians(60.) #Change this: Use the material to get the angle.
projx = ma.cos(angle)
projy = ma.sin(angle)
x = []
y = []
for comp1 in np.linspace(0., norm(material.cell[0]), potential.shape[0]):
    for comp2 in np.linspace(0., norm(material.cell[1]), potential.shape[1]):
        x.append(comp1 + projx*comp2)
        y.append(projy*comp2)
X = np.array(x).reshape(potential.shape[:2])
Y = np.array(y).reshape(potential.shape[:2])
# print(X)

# Plot a slice of the potential.
fig = plt.figure()
ax = plt.axes(projection="3d")
ax.plot_surface(X, Y, potential[:,:,vertical_index])
plt.show()

# Plot a slice of the vector field.
new_shape = (field.shape[0], field.shape[1])
U = np.zeros(new_shape)
V = np.zeros(new_shape)
for i in range(field.shape[0]):
    for j in range(field.shape[1]):
        U[i, j] = field[i, j, vertical_index, 0]
        V[i, j] = field[i, j, vertical_index, 1]
plt.quiver(X, Y, U, V)
plt.show()


# field = np.array([[[[1., 2., 1.], [2., 3., 4.],
#                     [0., 3., 2.], [3., 1., 0.]],
#                    [[1., 2., 1.], [2., 3., 4.],
#                     [0., 3., 2.], [3., 1., 0.]]]])
#                   # [[[1., 2., 1.], [2., 3., 4.],
#                   #  [0., 3., 2.], [3., 1., 0.]],
#                   # [[1., 2., 1.], [2., 3., 4.],
#                   #  [0., 3., 2.], [3., 1., 0.]]]])
# len1 = 2
# len2 = 2
# len3 = 2
# axis1 = np.linspace(0., 1., len1)
# axis2 = np.linspace(0., 1., len2)
# axis3 = np.linspace(0., 1., len3)
#
# print(field.shape)


# len1 = field.shape[0]//4
# len2 = field.shape[1]//4
# len3 = field.shape[2]//120
# axis1 = np.linspace(0., norm(material.cell[0]), len1)
# axis2 = np.linspace(0., norm(material.cell[1]), len2)
# axis3 = np.linspace(0., norm(material.cell[2]), len3)
# reduced_field = np.zeros((len1, len2, len3, 3))
# angle = ma.radians(60.) #Change this: Use the material to get the angle.
# projx = ma.cos(angle)
# projy = ma.sin(angle)
# x = []
# y = []
# z = []
# for i in range(len1):
#     for j in range(len2):
#         for k in range(len3):
#             comp1 = axis1[i]
#             comp2 = axis2[j]
#             comp3 = axis3[k]
#             x.append(comp1 + projx*comp2)
#             y.append(projy*comp2)
#             z.append(comp3)
#             reduced_field[i, j, k] = field[4*i, 4*j, 4*k, :]
# field = reduced_field
# X = np.array(x).reshape(field.shape[:3])
# Y = np.array(y).reshape(field.shape[:3])
# Z = np.array(z).reshape(field.shape[:3])
#
# ax = plt.figure().add_subplot(projection="3d")
# ax.quiver(X, Y, Z, field[:,:,:,0], field[:,:,:,1], field[:,:,:,2],
#           length=.3, normalize=True)
# plt.show()
