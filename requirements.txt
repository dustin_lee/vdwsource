matplotlib==3.3
numpy==1.19
scipy==1.5
ase==3.22
gpaw==20.1
